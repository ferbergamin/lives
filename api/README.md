API em Node.js + Express feita em live

[Cloud](https://api-piada.livedomaybe.now.sh/)

Tecnologias utilizadas
- [Express](https://expressjs.com/pt-br/4x/api.html)
- [Node](https://nodejs.org/en/docs/)
- [Mongoose](https://mongoosejs.com/docs/)