// O que é um controller
// Nesse contexto, o controller será nosso back-end e ira controlar os eventos
// requisitados na nossa api
const PiadaModel = require("../models/piadaModel");

//Controller que vai retornar uma piada
exports.getPiada = function(req, res){

    let id = req.params.piadaId;

    PiadaModel.findOne( { "_id":id }, function(err, data){
        if(err){
            res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
        if(!data){
            return res.status(406).json({
                'response':  null,
                'error': 'Piada não encontrada'
            });
            } else {
                return res.status(201).json({
                    error:null,
                    'response': {
                        'data': data
                    }
                });
            }
        }
    });
}

exports.getAllPiadas = function(req, res){
    PiadaModel.find({}, function(err, data){
        if(err){
            res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            res.status(201).json({
                'response': {
                    'data': data
                },
                'error': null
            })
        }
    });
}

exports.createPiada = function(req, res){
    let body = req.body;

    let data = {
        usuario: null,
        piada: null
    }

    for(let any in body){
        if(data.hasOwnProperty(any)){
            data[any] = body[any];
        }
    }

    if(data.usuario == undefined || data.piada == undefined){
        return res.status(406).json({
            'response': null,
            'error': 'Parâmetros inválidos'
        });
    } else {
        let newPiada = new PiadaModel(data);
        newPiada.save(function(err, piada){
            if(err){
                return res.status(406).json({
                    'response': null,
                    'error': err
                });
            } else {
                res.status(201).json({
                    'error':null,
                    'response': {
                        "data": piada
                    }
                });
            }
        });
    }
}

exports.deletePiada = function(req, res){
    let piadaId = req.params["piadaId"];

    PiadaModel.deleteOne({_id: piadaId}, function(err, data){
        if(err){
            return res.status(406).json({
                'response': null,
                'error': err
            });
        } else {
            if(!data){
                return res.status(406).json({
                    'response': null,
                    'error': 'Piada não encontrada'
                });
            } else {
                return res.status(201).json({
                    'error': null,
                    'response': {
                        "data": {
                            'message': 'Piada deletada com sucesso!'
                        }
                    }
                });
            }
        }
    });
}