//Vai importar o express
const express = require('express');

//vai instanciar o express
const app = express();

//Não dar problema de CORS
const cors = require("cors");

//Não dar problema no parser do body
const bodyParser = require('body-parser');

//importar o mongoose *biblioteca MongoDB*
const mongo = require("mongoose");

//Conectar ao cluster do mongo atlas
mongo.connect("mongodb+srv://aolive:aolive123@piadaslive-si3fv.mongodb.net/test?retryWrites=true&w=majority", {useNewUrlParser: true});


//Colocar o cors no nosso app
app.use(cors());
//Colocar o bodyParser no app
app.use(bodyParser());

//Definindo host
let host = '127.0.0.1';

//Definindo a porta
let port = process.env.PORT || 3000;

let piadaRouter = require("./routes/piadaRoute");

//Adicionando uma rota '/' com calback retornando Json com message:Hello World
app.get("/", function(req, res){
    res.status(201).json({message: "Hello word"});
});

app.use("/piadas", piadaRouter);

//201 e o 200 => Sucedida
//300 ou 301 => SUcedida mas com warnings
//401 Não autorizado
//406 erro
//404 página não encontrada
//500 Erro total na aplicação ou algo não encontrado

//Iniciando o express na rota host:port
app.listen(port, host, () => {
    //Calback
    console.log(`Listenng on http://${host}:${port}`);
});