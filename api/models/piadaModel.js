//O que é um model?
//Modelo nesse contexto será um Documento ou Esquema (Schema) do Mongo
//Aqui definiremos quais campos esse schema terá e quais seus tipos e parametros

//Importar o Schema do Mongo
const Schema = require("mongoose").Schema;

//Importando a função model do Mongo
const model = require("mongoose").model;


//Criando nosso modelo
const PiadaModel = new Schema({
    usuario: { type: String },
    piada: { type: String }
});


//Exportando modelo
module.exports = model("Piadas", PiadaModel);