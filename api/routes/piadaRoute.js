const Router = require('express').Router();

const piadaController = require('../crontrollers/piadaController');

Router.get("/listar", piadaController.getAllPiadas);

Router.get("/listar/:piadaId", piadaController.getPiada);

Router.post("/criar-piada", piadaController.createPiada);

Router.delete("/deletar/:piadaId", piadaController.deletePiada);

module.exports = Router;

//Requerir get("127.0.0.1:3000/piadas/listar")